# Write a function that meets these requirements.
#
# Name:       group_cities_by_state
# Parameters: a list of cities in the format "«name», «st»"
#             where «name» is the name of the city, followed
#             by a comma and a space, then the two-letter
#             abbreviation of the state
# Returns:    a dictionary whose keys are the two letter
#             abbreviations of the states in the list and
#             whose values are a list of the cities appearing
#             in that list for that state
#
# In the items in the input, there will only be one comma.
#
# Examples:
#     * input:   ["San Antonio, TX"]
#       returns: {"TX": ["San Antonio"]}
#     * input:   ["Springfield, MA", "Boston, MA"]
#       returns: {"MA": ["Springfield", "Boston"]}
#     * input:   ["Cleveland, OH", "Columbus, OH", "Chicago, IL"]
#       returns: {"OH": ["Cleveland", "Columbus"], "IL": ["Chicago"]}
#
# You may want to look up the ".strip()" method for the string.

def group_cities_by_state(items):
    new_dict = {}
    for item in items:
        item = item.split(',')

        if item[1] in new_dict:
            for key, value in new_dict.items():
                # print('key', key)
                # print('value before', value)
                value.append(item[0])
                # print('value after', value)
        else:
            new_dict[item[1]] = [item[0]]
    return new_dict


print(group_cities_by_state(["San Antonio, TX"]))
print(group_cities_by_state(["Springfield, MA", "Boston, MA"]))
print(group_cities_by_state(["Cleveland, OH", "Columbus, OH", "Chicago, IL"]))
