# Complete the check_password function that accepts a
# single parameter, the password to check.
#
# A password is valid if it meets all of these criteria
#   * It must have at least one lowercase letter (a-z)
#   * It must have at least one uppercase letter (A-Z)
#   * It must have at least one digit (0-9)
#   * It must have at least one special character $, !, or @
#   * It must have six or more characters in it
#   * It must have twelve or fewer characters in it
#
# The string object has some methods that you may want to use,
# like ".isalpha", ".isdigit", ".isupper", and ".islower"

def check_password(password):
    accepted_special_characters = ['$', '!', '@']
    upper = upper_letters(password)
    lower = lower_letters(password)
    digit = digits(password)
    special_chars = special_characters(password, accepted_special_characters)
    length_of_password = find_length_of_password(password)

    return upper and lower and digit and special_chars and length_of_password


def find_length_of_password(password):
    if len(password) >= 6 and len(password) <= 12:
        return True
    return False


def special_characters(password, accepted_special_characters):
    for letter in password:
        if letter in accepted_special_characters:
            return True
    return False


def digits(password):
    for letter in password:
        if letter.isdigit():
            return True
    return False


def upper_letters(password):
    for letter in password:
        if letter.isupper():
            return True
    return False


def lower_letters(password):
    for letter in password:
        if letter.islower():
            return True
    return False


print(check_password('whatever12!3222222222'))
print(check_password('PAord!123'))
print(check_password('whatever123'))
print(check_password('@@@@@@@@'))
