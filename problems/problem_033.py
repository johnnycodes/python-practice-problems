# Complete the sum_of_first_n_even_numbers function which
# accepts a numerical count n and returns the sum of the
# first n even numbers
#
# If the value of the limit is less than 0, then it should
# return None
#
# Examples:
#   * -1 returns None
#   * 0 returns 0
#   * 1 returns 0+2=2
#   * 2 returns 0+2+4=6
#   * 5 returns 0+2+4+6+8+10=30
#
# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def sum_of_first_n_even_numbers(n):
    if n < 0:
        return None
    list_of_evens = []
    for i in range((n+1)**2):
        if i % 2 == 0:
            list_of_evens.append(i)
    list_of_evens = list_of_evens[:n+1]
    x = f''
    for num in list_of_evens:
        x += f'{num}+'
    x = x[:-1] + "=" + str(sum(list_of_evens))
    # total = 0
    # x = f''
    # for i in range(n):
    #     if i % 2 == 0:
    #         total += i
    #         x += f'{i}+'
    # x = x[:-1] + "=" + str(total)
    return x

print(sum_of_first_n_even_numbers(-1))
print(sum_of_first_n_even_numbers(0))
print(sum_of_first_n_even_numbers(1))
print(sum_of_first_n_even_numbers(5))
