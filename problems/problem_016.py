# Complete the is_inside_bounds function which takes an x
# coordinate and a y coordinate, and then tests each to
# make sure they're between 0 and 10, inclusive.

def is_inside_bounds(x, y):
    if x in range(1,11) and y in range(1, 11):
        return True
    else:
        return False

print(is_inside_bounds(2, 2))
print(is_inside_bounds(2, 12))
print(is_inside_bounds(12, 2))
print(is_inside_bounds(12, 12))
print(is_inside_bounds(10, 10))
print(is_inside_bounds(0, 10))
