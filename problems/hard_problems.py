def total_song_knowers(num_villagers, attendees_lists):
    sorted_attendees = []
    for day in attendees_lists:
        day.sort()
        sorted_attendees.append(day)
    total_songs = number_of_songs(attendees_lists)

    knowledge_key = {}

    for day in sorted_attendees:
        if day[0] == 0:  # if visitor is present the attendees learn a new song
            day.remove(0)
            for attendee in day:
                if attendee in knowledge_key.keys():
                    knowledge_key[attendee] += 1
                else:
                    knowledge_key[attendee] = 1
            print(day)
        else:  # if visitor isn't present, everyone shares the song so set each key to max value
            for attendee in day:
                if attendee in knowledge_key:
                    knowledge_key[attendee] = max(knowledge_key.values())
                else:
                    knowledge_key[attendee] = max(knowledge_key.values())

    print('knowledge key', knowledge_key)

    print('total songs', total_songs)
    who_knows_everything = 0
    for key, value in knowledge_key.items():
        if value == total_songs:
            who_knows_everything += 1

    return who_knows_everything

# def learn_original_song(sorted_attendees):
#     for day in sorted_attendees:
#         if day[0] == 0:
#             for attendee in day:
def number_of_songs(attendees_list):
    count = 0
    for day in attendees_list:
        for attendee in day:
            if attendee == 0:
                count += 1
    return count


print(total_song_knowers(3, [[0, 1], [1, 2, 3], [3, 1, 0]]))

# Refactor above code


def total_song_knowers2(num_villagers, attendees_lists):
    print('refactored function')
    sorted_attendees = sort_attendees_list(attendees_lists)
    total_number_unique_songs = number_of_songs(attendees_lists)
    attendee_knows_num_of_songs = find_songs_known_by_attendees(sorted_attendees)
    count_who_knows_all_songs = who_knows_songs(attendee_knows_num_of_songs, total_number_unique_songs)
    print('total songs', total_number_unique_songs)
    print('attendee knows num of songs', attendee_knows_num_of_songs)
    return count_who_knows_all_songs


def who_knows_songs(knowledge_key, total_songs):
    count = 0
    for value in knowledge_key.values():
        if value == total_songs:
            count += 1
    print('count', count)
    return count


def find_songs_known_by_attendees(sorted_attendees):
    knowledge_key = {}
    for day in sorted_attendees:
        if is_visitor_present(day):
            day.remove(0)  # this is why we sorted, visitor is 0 index
            for attendee in day:
                add_attendee_song_if_visitor(attendee, knowledge_key)
        else:
            for attendee in day:
                add_attendee_song_if_not_visitor(attendee, knowledge_key)
    return knowledge_key


def add_attendee_song_if_not_visitor(attendee, knowledge_key):
    if attendee in knowledge_key.keys():
        knowledge_key[attendee] = max(knowledge_key.values())
    else:
        knowledge_key[attendee] = max(knowledge_key.values())
    return knowledge_key


def add_attendee_song_if_visitor(attendee, knowledge_key):
    if attendee in knowledge_key.keys():
        knowledge_key[attendee] += 1
    else:
        knowledge_key[attendee] = 1
    return knowledge_key


def is_visitor_present(present):
    if present[0] == 0:
        return True


def sort_attendees_list(attendees_lists):
    sorted_attendees = []
    for day in attendees_lists:
        day.sort()
        sorted_attendees.append(day)
    return sorted_attendees


def number_of_songs(attendees_list):
    count = 0
    for day in attendees_list:
        for attendee in day:
            if attendee == 0:
                count += 1
    return count


print(total_song_knowers2(3, [[0, 1], [1, 2, 3], [3, 1, 0]]))
print(total_song_knowers2(4, [[0, 1], [0, 1], [0, 2], [0, 2], [1, 3], [3, 2], [3, 4]]))




"""
Problem 2
"""

def translate(words, digits):
    keypad_dict = {
        2: ['A', 'B', 'C'],
        3: ['D', 'E', 'F'],
        4: ['G', 'H', 'I'],
        5: ['J', 'K', 'L'],
        6: ['M', 'N', 'O'],
        7: ['P', 'Q', 'R', 'S'],
        8: ['T', 'U', 'V'],
        9: ['W', 'X', 'Y', 'Z']
    }

    digit_list = [int(x) for x in str(digits)]
    print('digit_list', digit_list)

    keypad_letter_list = [keypad_dict[x] for x in digit_list]
    print('keypad_letter_list', keypad_letter_list)

    possible_words = []

    for word in words:
        print('word', word)
        for i in range(len(word)):
            print('i', i)
            if word[i] in keypad_letter_list[i]:
                print(word[i])
                possible_words.append(word)
            else:
                break

    print('possible words', possible_words)

    possible_words = [*set(possible_words)]
    print('possible words', possible_words)

    possible_words = len(possible_words)
    print('possible words', possible_words)

    return possible_words

print(translate(['TOMO', 'MONO', 'DAK'], 6666))
print(translate(['DOM', 'FON', 'TOM'], 366))
print(translate(['JA', 'LA'], 52))


# for i in range(len(nums)):
#     print('i', i)
#     for num in nums:
#         for j in range(len(nums[i:])):
#             print(nums[i] + nums[i+j])


# steps = [[2, 4], [1], [1], [1, 2]]
# steps_copy = [[2, 4], [1], [1], [1, 2]]
# for index, step in enumerate(steps):
#     # print(f'index {(index)}, step {step}')
#     x = index + 1
#     # print('index + 1', x)
#     # try:
#     if len(step) == 1:
#         # print('in len == 1 if statement')
#         for value in step:
#             # print('value', value)
#             # print('steps of x', steps[x])
#             # print('x', x)
#             # print('step of x', steps[x].pop())
#             print('sum of the values', value + int(steps[x].pop()))
#             steps = steps_copy
#     else:
#         print('pass', index, step)
#     # except:
#     #     pass
