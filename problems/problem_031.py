# Complete the sum_of_squares function which accepts
# a list of numerical values and returns the sum of
# each item squared
#
# If the list of values is empty, the function should
# return None
#
# Examples:
#   * [] returns None
#   * [1, 2, 3] returns 1*1+2*2+3*3=14
#   * [-1, 0, 1] returns (-1)*(-1)+0*0+1*1=2
#
# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def sum_of_squares(values):
    if not values:
        return None
    total = 0
    x = f""
    for num in values:
        x = x + f"{num}*{num}+"
        total += num * num
    x = x[:len(x)-2] + "=" + str(total)
    return x

print(sum_of_squares([1,2,3]))
print(sum_of_squares([1,2,3,4]))
print(sum_of_squares([1,2,3,4,5]))
print(sum_of_squares([]))

def sum_of_squares(values):
    total = 0
    if len(values) == 0:
        return None
    for num in values:
        total += (num * num)
    return total

print(sum_of_squares([1,2,3]))
print(sum_of_squares([1,2,3,4]))
print(sum_of_squares([1,2,3,4,5]))
print(sum_of_squares([]))
