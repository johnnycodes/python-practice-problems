# Complete the find_second_largest function which accepts
# a list of numerical values and returns the second largest
# in the list
#
# If the list of values is empty, the function should
# return None
#
# If the list of values has only one value, the function
# should return None
#
# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def find_second_largest(values):
    if not values or len(values) == 1:
        return None
    x = sorted(values)[-2:-1].pop()
    return x


print(find_second_largest([1, 2, 30, 4, 5]))
print(find_second_largest([1, 2, 30, 4, 5, 100, 1000, 1, 2, 3]))
print(find_second_largest([1]))
print(find_second_largest([]))


def find_second_largest(values):
    if not values or len(values) == 1:
        return None
    # values.sort()
    return sorted(values)[-2:-1].pop()


print(find_second_largest([1, 2, 30, 4, 5]))
print(find_second_largest([1, 2, 30, 4, 5, 100, 1000, 1, 2, 3]))
print(find_second_largest([1]))
print(find_second_largest([]))
