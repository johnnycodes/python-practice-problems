# Complete the calculate_average function which accepts
# a list of numerical values and returns the average of
# the numbers.
#
# If the list of values is empty, the function should
# return None
#
# Pseudocode is available for you

def calculate_average(values):
    if not values:
        return None
    return sum(values) / len(values)




print(calculate_average([]))
print(calculate_average([1,2,3]))
print(calculate_average([1,2,3,10]))
