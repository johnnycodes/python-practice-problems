# Complete the max_in_list function to find the
# maximum value in a list
#
# If the list is empty, then return None.
#

def max_in_list(values):
    return max(values)


print(max_in_list([1,2,3]))
print(max_in_list([1,20,3]))
print(max_in_list([10,2,3]))
